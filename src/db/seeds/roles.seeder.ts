import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { DataSource } from 'typeorm';
import { Role } from '../../roles/roles.entity';

export default class RolesSeeder implements Seeder {
  public async run(dataSource: DataSource, factoryManager: SeederFactoryManager): Promise<void> {

    await dataSource.query('TRUNCATE "role" RESTART IDENTITY CASCADE;');

    const repository = dataSource.getRepository(Role);

    const adminRole = repository.create({
        value: 'ADMIN',
        description: 'Administrator'
    })

    const userRole = repository.create({
        value: 'USER',
        description: 'Common user'
    })

    await repository.insert(adminRole);
    await repository.insert(userRole);
  }
}