import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { DataSource } from 'typeorm';
import { User } from '../../users/users.entity';
import * as bcrypt from 'bcryptjs';
import { Role } from '../../roles/roles.entity';

export default class UsersSeeder implements Seeder {
  public async run(dataSource: DataSource, factoryManager: SeederFactoryManager): Promise<void> {

    await dataSource.query('TRUNCATE "user" RESTART IDENTITY CASCADE;');

    const repository = dataSource.getRepository(User);
    const rolesRepository = dataSource.getRepository(Role);
    const adminRole = await rolesRepository.findOne({where: {value: 'ADMIN'}})
    const userRole = await rolesRepository.findOne({where: {value: 'USER'}})

    const adminUser = repository.create({
        email: 'admin@email.com',
        password: await bcrypt.hash('admin', 5),
        roles: [adminRole, userRole]
    })

    await repository.save(adminUser);
  }
}