import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './tasks.entity';
import { ListsService } from 'src/lists/lists.service';
import { Repository } from 'typeorm';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { UpdateTaskSortingDto } from './dto/update-task-sorting.dto';
import { UpdateTaskListDto } from './dto/update-task-list.dto';

@Injectable()
export class TasksService {

    constructor(@InjectRepository(Task) private tasksRepository: Repository<Task>,
                private listsService: ListsService) {}

    async getTasksList(userId:number, listId: number) {
        return await this.tasksRepository.find({
            where: {
                list: {
                    id: listId,
                    project: {
                        user: {id: userId}
                    } 
                }                      
            },
            order: {
                sortIndex: "ASC"
            },
            relations: {
                comments: true
            },
        })
    }

    async getTaskById(userId: number, id: number) {
        return await this.tasksRepository.findOne({
            where: {
                id: id,
                list: {
                    project: {
                        user: {id: userId}
                    } 
                }
            },
            relations: {
                list: {
                    project: true
                },
                comments: true
            },
        })
    }

    async createTask(userId: number, listId: number, createTaskDto: CreateTaskDto) {
        const newTask = this.tasksRepository.create(createTaskDto)
        const list = await this.listsService.getListById(userId, listId)
        newTask.sortIndex = await this.getNextSortingIndex(list.id)

        await this.updateSorting(list.id, -1, newTask.sortIndex)

        newTask.list = list
        await this.tasksRepository.save(newTask)

        return newTask
    }

    async updateTask(userId: number, id: number, updateTaskDto: UpdateTaskDto) {
        const task = await this.getTaskById(userId, id) 
        const updatedTask: Task = {...task, ...updateTaskDto}

        await this.tasksRepository.save(updatedTask)
        
        return updatedTask
    }

    // TODO: limit maximum sorting index
    async updateTaskSorting(userId: number, id: number, updateSortingDto: UpdateTaskSortingDto) {
        const task = await this.getTaskById(userId, id) 
        const oldIndex = task.sortIndex
        task.sortIndex = updateSortingDto.sortIndex

        await this.updateSorting(task.list.id, oldIndex, task.sortIndex)

        await this.tasksRepository.save(task)
        
        return task
    }

    async updateTaskList(userId: number, id: number, updateListDto: UpdateTaskListDto) {

        const task = await this.getTaskById(userId, id) 
        const oldListId = task.list.id
        const oldIndex = task.sortIndex

        const newList = await this.listsService.getListById(userId, updateListDto.listId)
        if(newList.project.id !== task.list.project.id) { return task }

        task.list = newList
        task.sortIndex = await this.getNextSortingIndex(task.list.id)

        await this.updateSorting(task.list.id, -1, task.sortIndex)

        await this.tasksRepository.save(task)

        await this.updateSorting(oldListId, oldIndex, -1)
        
        return task
    }

    async deleteTask(userId: number, id: number) {
        const task = await this.getTaskById(userId, id)
        if(!task) {
            return []
        }

        const listId = task.list.id
        const oldIndex = task.sortIndex
        await this.tasksRepository.remove(task)
        await this.updateSorting(listId, oldIndex, -1)

        return this.getTasksList(userId, listId) 
    }
    
    private async updateSorting(listId: number, oldIndex: number, newIndex: number) {

        if(oldIndex === newIndex || oldIndex === -1) { return } 

        const query = this.tasksRepository
            .createQueryBuilder()
            .update(Task);

        //delete or move down
        if(newIndex === -1 || newIndex > oldIndex) {
            query
                .set({
                    sortIndex: () => "sortIndex - 1"
                })
                .where("sortIndex > :oldIndex", {oldIndex});

            if(newIndex > oldIndex) {
                query
                    .andWhere("sortIndex <= :newIndex", {newIndex});
            }
        } else if(newIndex < oldIndex) { //move up
            query
                .set({
                    sortIndex: () => "sortIndex + 1"
                })
                .where("sortIndex >= :newIndex", {newIndex});

            if(newIndex < oldIndex) {
                query.andWhere("sortIndex < :oldIndex", {oldIndex});
            }
        }

        await query
            .andWhere("list.id = :listId", {listId})
            .execute();
    }

    private async getNextSortingIndex(listId: number) {
        const maxIndex = await this.tasksRepository
            .createQueryBuilder("tasks")
            .select("MAX(tasks.sortIndex)", "value")
            //.from(List, "lists")
            .where("tasks.list.id = :listId", {listId})
            .getRawOne();

        return maxIndex.value + 1 // null + 1 works fine for now
    }
}
