import { ApiProperty } from "@nestjs/swagger";
import { List } from "../lists/lists.entity";
import { Comment } from "../comments/comments.entity";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Task {

    @ApiProperty({example: 1})
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({example: "Switch to another ORM"})
    @Column()
    name: string;

    @ApiProperty({example: "Try out Sequilize"})
    @Column()
    description: string;

    @ApiProperty({example: 1})
    @Column({default: 1})
    sortIndex: number;

    @ApiProperty({example: "2024-06-20T08:08:00.191Z"})
    @CreateDateColumn()
    createdAt: Date;

    @ApiProperty({example: "2024-06-20T08:08:00.191Z"})
    @UpdateDateColumn()
    updatedAt: Date;

    //@ApiProperty({type: () => List})
    @ManyToOne(() => List, (list) => list.tasks, { onDelete: "CASCADE" })
    @JoinColumn()
    list: List

    @ApiProperty({type: () => [Comment]})
    @OneToMany(() => Comment, (comment) => comment.task)
    comments: Comment[]
}