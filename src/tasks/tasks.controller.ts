import { Body, Controller, Delete, Get, Param, Post, Put, Request } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { TasksService } from './tasks.service';
import { Roles } from 'src/auth/roles-auth.decorator';
import { Task } from './tasks.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskSortingDto } from './dto/update-task-sorting.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { UpdateTaskListDto } from './dto/update-task-list.dto';

@ApiTags("Tasks")
@ApiBearerAuth()
@Controller() //'projects/:projectId/lists/:listId/tasks'
export class TasksController {

    constructor(private tasksService: TasksService) {}

    @ApiOperation({summary: "List tasks"})
    @ApiParam({name: "listId", type: "number", required: true, description: "List id"})
    @ApiOkResponse({type: [Task]})
    @Get('lists/:listId/tasks')
    @Roles(["USER"])
    getAll(@Request() req: any, @Param('listId') listId: number) {
        return this.tasksService.getTasksList(req.user.id, listId)
    }

    @ApiOperation({summary: "Get task by id"})
    @ApiParam({name: "id", type: "number", required: true, description: "Task id"})
    @ApiOkResponse({type: Task})
    @Get("tasks/:id")
    @Roles(["USER"])
    getById(@Request() req: any, @Param('id') id: number) {
        return this.tasksService.getTaskById(req.user.id, id)
    }

    @ApiOperation({summary: "Create task"})
    @ApiParam({name: "listId", type: "number", required: true, description: "List id"})
    @ApiCreatedResponse({type: Task})
    @Post('lists/:listId/tasks')
    @Roles(["USER"])
    create(@Request() req: any, @Param('listId') listId: number, @Body() createTaskDto: CreateTaskDto) {
        return this.tasksService.createTask(req.user.id, listId, createTaskDto)
    }

    @ApiOperation({summary: "Update task"})
    @ApiParam({name: "id", type: "number", required: true, description: "Task id"})
    @ApiOkResponse({type: Task})
    @Put("tasks/:id")
    @Roles(["USER"])
    update(@Request() req: any, @Param('id') id: number, @Body() updateTaskDto: UpdateTaskDto) {
        return this.tasksService.updateTask(req.user.id, id, updateTaskDto)
    }

    @ApiOperation({summary: "Update task sorting index"})
    @ApiParam({name: "id", type: "number", required: true, description: "Task id"})
    @ApiOkResponse({type: Task})
    @Put("tasks/:id/sort")
    @Roles(["USER"])
    updateSorting(@Request() req: any, @Param('id') id: number, @Body() updateTaskSortingDto: UpdateTaskSortingDto) {
        return this.tasksService.updateTaskSorting(req.user.id, id, updateTaskSortingDto)
    }

    @ApiOperation({summary: "Change task list"})
    @ApiParam({name: "id", type: "number", required: true, description: "Task id"})
    @ApiOkResponse({type: Task})
    @Put("tasks/:id/list")
    @Roles(["USER"])
    updateList(@Request() req: any, @Param('id') id: number, @Body() updateTaskListDto: UpdateTaskListDto) {
        return this.tasksService.updateTaskList(req.user.id, id, updateTaskListDto)
    }

    @ApiOperation({summary: "Delete task"})
    @ApiParam({name: "id", type: "number", required: true, description: "Task id"})
    @ApiOkResponse({type: [Task]})
    @Delete("tasks/:id")
    @Roles(["USER"])
    delete(@Request() req: any, @Param('id') id: number) {
        return this.tasksService.deleteTask(req.user.id, id)
    }
}
