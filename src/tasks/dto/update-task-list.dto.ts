import { ApiProperty } from "@nestjs/swagger";
import { IsInt } from "class-validator";

export class UpdateTaskListDto {

    @ApiProperty({example: "1"})
    @IsInt({message: "Must be a valid ID for list in the same project"})
    readonly listId: number;
}