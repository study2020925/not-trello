import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";

export class UpdateTaskDto {

    @ApiProperty({example: "Switch to another ORM", required: false})
    @IsOptional()
    @IsString({message: "Must be a string"})
    readonly name?: string;

    @ApiProperty({example: "Try out Sequilize", required: false})
    @IsOptional()
    @IsString({message: "Must be a string"})
    readonly description?: string;
}