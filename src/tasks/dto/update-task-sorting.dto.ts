import { ApiProperty } from "@nestjs/swagger";
import { IsInt, Min } from "class-validator";

export class UpdateTaskSortingDto {

    @ApiProperty({example: "1"})
    @IsInt({message: "Must be a number greater than 1"})
    @Min(1)
    readonly sortIndex: number;
}