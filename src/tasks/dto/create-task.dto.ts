import { ApiProperty } from "@nestjs/swagger";
import { IsInt, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class CreateTaskDto {

    @ApiProperty({example: "Switch to another ORM"})
    @IsNotEmpty({message: "Required"})
    @IsString({message: "Must be a string"})
    readonly name: string;

    @ApiProperty({example: "Try out Sequilize", required: false})
    @IsOptional()
    @IsString({message: "Must be a string"})
    readonly description?: string;
}