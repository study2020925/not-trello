import { Body, Controller, Delete, Get, Param, Post, Put, Request } from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { Project } from './projects.entity';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/auth/roles-auth.decorator';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';

@ApiTags("Projects")
@ApiBearerAuth()
@Controller('projects')
export class ProjectsController {

    constructor(private projectsService: ProjectsService) {}

    @ApiOperation({summary: "List projects"})
    @ApiOkResponse({type: [Project]})
    @Get()
    @Roles(["USER"])
    getAll(@Request() req: any) {
        return this.projectsService.getProjectsList(req.user.id)
    }

    @ApiOperation({summary: "Get project by id"})
    @ApiParam({name: "id", type: "number", required: true, description: "Project id"})
    @ApiOkResponse({type: Project})
    @Get("/:id")
    @Roles(["USER"])
    getById(@Request() req: any, @Param('id') id: number) {
        return this.projectsService.getProjectById(req.user.id, id)
    }

    @ApiOperation({summary: "Create project"})
    @ApiCreatedResponse({type: Project})
    @Post()
    @Roles(["USER"])
    create(@Request() req: any, @Body() createProjectDto: CreateProjectDto) {
        return this.projectsService.createProject(req.user.id, createProjectDto)
    }

    @ApiOperation({summary: "Update project"})
    @ApiParam({name: "id", type: "number", required: true, description: "Project id"})
    @ApiOkResponse({type: Project})
    @Put("/:id")
    @Roles(["USER"])
    update(@Request() req: any, @Param('id') id: number, @Body() updateProjectDto: UpdateProjectDto) {
        return this.projectsService.updateProject(req.user.id, id, updateProjectDto)
    }

    @ApiOperation({summary: "Delete project"})
    @ApiParam({name: "id", type: "number", required: true, description: "Project id"})
    @ApiOkResponse({type: [Project]})
    @Delete("/:id")
    @Roles(["USER"])
    delete(@Request() req: any, @Param('id') id: number) {
        return this.projectsService.deleteProject(req.user.id, id)
    }
}