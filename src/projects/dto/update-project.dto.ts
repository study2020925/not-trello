import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";

export class UpdateProjectDto {

    @ApiProperty({example: "not-trello", required: false})
    @IsOptional()
    @IsString({message: "Must be a string"})
    readonly name?: string;

    @ApiProperty({example: "Study project using NestJs", required: false})
    @IsOptional()
    @IsString({message: "Must be a string"})
    readonly description?: string;
}