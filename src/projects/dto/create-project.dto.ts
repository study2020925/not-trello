import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString } from "class-validator";

export class CreateProjectDto {

    @ApiProperty({example: "not-trello"})
    @IsNotEmpty({message: "Required"})
    @IsString({message: "Must be a string"})
    readonly name: string;

    @ApiProperty({example: "Study project using NestJs", required: false})
    @IsOptional()
    @IsString({message: "Must be a string"})
    readonly description?: string;
}