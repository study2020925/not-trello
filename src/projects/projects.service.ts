import { Injectable } from '@nestjs/common';
import { Project } from './projects.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProjectDto } from './dto/create-project.dto';
import { UsersService } from 'src/users/users.service';
import { UpdateProjectDto } from './dto/update-project.dto';

@Injectable()
export class ProjectsService {

    constructor(@InjectRepository(Project) private projectsRepository: Repository<Project>,
                private usersService: UsersService) {}

    async getProjectsList(userId: number) {
        return await this.projectsRepository.find({
            where: {
                user: {id: userId}
            },
            relations: {
               lists: {tasks: {comments: true}},
            },
        })
    }

    async getProjectById(userId: number, id: number) {
        return await this.projectsRepository.findOne({
            where: {
                id,
                user: {id: userId}
            },
            relations: {
                lists: {tasks: {comments: true}},
            },
        })
    }

    async createProject(userId: number, createProjectDto: CreateProjectDto) {
        const newProject = this.projectsRepository.create(createProjectDto)
        const user = await this.usersService.getUserById(userId)

        newProject.user = user
        await this.projectsRepository.save(newProject)

        return newProject
    }

    async updateProject(userId: number, id: number, updateProjectDto: UpdateProjectDto) {
        const project = await this.getProjectById(userId, id) 
        const updatedProject: Project = {...project, ...updateProjectDto}

        await this.projectsRepository.save(updatedProject)
        
        return updatedProject
    }

    async deleteProject(userId: number, id: number) {
        const project = await this.getProjectById(userId, id)
        await this.projectsRepository.remove(project)

        return this.getProjectsList(userId) 
    }
}
