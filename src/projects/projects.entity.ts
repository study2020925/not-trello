import { ApiProperty } from "@nestjs/swagger";
import { List } from "../lists/lists.entity";
import { User } from "../users/users.entity";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Project {

    @ApiProperty({example: 1})
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({example: "not-trello"})
    @Column()
    name: string;

    @ApiProperty({example: "Study project using NestJs"})
    @Column()
    description: string;

    @ApiProperty({example: "2024-06-20T08:08:00.191Z"})
    @CreateDateColumn()
    createdAt: Date;

    @ApiProperty({example: "2024-06-20T08:08:00.191Z"})
    @UpdateDateColumn()
    updatedAt: Date;

    //@ApiProperty({type: () => [User]})
    @ManyToOne(() => User, (user) => user.projects, { onDelete: "CASCADE" })
    @JoinColumn()
    user: User

    @ApiProperty({type: () => [List]})
    @OneToMany(() => List, (list) => list.project)
    lists: List[]
}