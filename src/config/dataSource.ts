import { Role } from '../roles/roles.entity';
import { User } from '../users/users.entity';
import { DataSourceOptions } from 'typeorm';
import { SeederOptions } from 'typeorm-extension';
import UsersSeeder from '../db/seeds/users.seeder';
import RolesSeeder from '../db/seeds/roles.seeder';
import { Project } from '../projects/projects.entity';
import { List } from '../lists/lists.entity';
import { Task } from '../tasks/tasks.entity';
import { Comment } from '../comments/comments.entity';

export const dataSourceOptions: DataSourceOptions & SeederOptions = {
    type: 'postgres',
    host: process.env.POSTGRES_HOST,
    port: +process.env.POSTGRES_PORT,
    username: process.env.POSTGRES_USERNAME,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DATABASE,
    entities: [User, Role, Project, List, Task, Comment],
    synchronize: true,
  
    seeds: [RolesSeeder, UsersSeeder],
};