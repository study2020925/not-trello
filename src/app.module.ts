import { Module } from "@nestjs/common";
import { UsersModule } from './users/users.module';
import { ConfigModule } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { RolesModule } from './roles/roles.module';
import { AuthModule } from './auth/auth.module';
import { RolesGuard } from "./auth/roles.guard";
import { APP_GUARD, APP_PIPE } from "@nestjs/core";
import { ValidationPipe } from "./pipes/validation.pipe";
import { ProjectsModule } from './projects/projects.module';
import { TasksModule } from './tasks/tasks.module';
import { ListsModule } from './lists/lists.module';
import { dataSourceOptions } from "./config/dataSource";
import { CommentsModule } from "./comments/comments.module";

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: ".env"
        }),
        TypeOrmModule.forRoot(dataSourceOptions),
        UsersModule,
        RolesModule,
        AuthModule,
        ProjectsModule,
        TasksModule,
        ListsModule,
        CommentsModule
    ],
    providers: [
        {
            provide: APP_GUARD,
            useClass: RolesGuard,
        },
        {
            provide: APP_PIPE,
            useClass: ValidationPipe
        }
    ]
})
export class AppModule {}