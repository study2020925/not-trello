import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class AssignRoleDto {

    @ApiProperty({example: 1})
    @IsNotEmpty({message: "Required"})
    @IsNumber({}, {message: "Must be a number"})
    readonly userId: number;

    @ApiProperty({example: "USER", description: "Role name"})
    @IsNotEmpty({message: "Required"})
    @IsString({message: "Must be a string"})
    readonly role: string;
}