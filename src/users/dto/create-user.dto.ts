import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString, Length } from "class-validator";

export class CreateUserDto {

    @ApiProperty({example: "user@email.com"})
    @IsNotEmpty({message: "Required"})
    @IsString({message: "Must be a string"})
    @IsEmail({}, {message: "Invalid email"})
    readonly email: string;

    @ApiProperty({example: "qwerty1234", description: "Between 4 and 16 characters long"})
    @IsNotEmpty({message: "Required"})
    @IsString({message: "Must be a string"})
    @Length(4, 16, {message: "Length must be between 4 and 16 characters"})
    readonly password: string;
}