import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './users.entity';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { RolesService } from 'src/roles/roles.service';
import * as bcrypt from 'bcryptjs';
import { AssignRoleDto } from './dto/assign-role.dto';

@Injectable()
export class UsersService {
    constructor(@InjectRepository(User) private usersRepository: Repository<User>,
            private rolesService: RolesService) {}

    async createUser(userDto: CreateUserDto) {
        const hashPassword = await bcrypt.hash(userDto.password, 5)
        const newUser = this.usersRepository.create({...userDto, password: hashPassword})
        const role = await this.rolesService.getRoleByValue("USER")
        newUser.roles = [role]
        await this.usersRepository.save(newUser)
        return newUser
    }

    async getUsersList() {
        return await this.usersRepository.find({
            relations: {
                roles: true,
            },
        })
    }

    async getUserByEmail(email: string) {
        return await this.usersRepository.findOne({
            where: {email}, 
            relations: {
                roles: true,
            },
        })
    }

    async getUserById(id: number) {
        return await this.usersRepository.findOne({
            where: {id}, 
            relations: {
                roles: true,
            },
        })
    }

    async assignRole(userRoleDto: AssignRoleDto) {
        const user = await this.usersRepository.findOne({
            where: {
                id: userRoleDto.userId
            },
            relations: {
                roles: true,
            },
        })
        const role = await this.rolesService.getRoleByValue(userRoleDto.role)

        if(!user || !role) {
            throw new HttpException("User and/or role not found", HttpStatus.BAD_REQUEST)
        }

        user.roles = [...user.roles, role]
        await this.usersRepository.save(user)
        return user
    }
}
