import { Body, Controller, Get, HttpStatus, Post } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { AssignRoleDto } from './dto/assign-role.dto';
import { Roles } from 'src/auth/roles-auth.decorator';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { User } from './users.entity';

@ApiTags("Users")
@ApiBearerAuth()
@Controller('users')
export class UsersController {
    
    constructor(private usersService: UsersService) {}

    @ApiOperation({summary: "Create user"})
    @ApiOkResponse({type: User})
    @Post()
    @Roles(["ADMIN"])
    create(@Body() userDto: CreateUserDto) {
        return this.usersService.createUser(userDto);
    }

    @ApiOperation({summary: "List users"})
    @ApiOkResponse({type: [User]})
    @Get()
    @Roles(["ADMIN"])
    getAll() {
        return this.usersService.getUsersList()
    }

    @ApiOperation({summary: "Assign role to user"})
    @ApiOkResponse({type: [User]})
    @Post("role")
    @Roles(["ADMIN"])
    assignRole(@Body() userRoleDto: AssignRoleDto) {
        return this.usersService.assignRole(userRoleDto);
    }
}
