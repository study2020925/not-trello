import { ApiProperty } from "@nestjs/swagger";
import { Project } from "../projects/projects.entity";
import { Role } from "../roles/roles.entity";
import { Column, CreateDateColumn, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class User {

    @ApiProperty({example: 1})
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({example: "user@email.com"})
    @Column()
    email: string;

    @ApiProperty({example: "qwerty1234", description: "Between 4 and 16 characters long"})
    @Column()
    password: string;

    @ApiProperty({example: true, description: "Is user currently active"})
    @Column({default: true})
    isActive: boolean;

    @ApiProperty({example: "2024-06-20T08:08:00.191Z"})
    @CreateDateColumn()
    createdAt: Date;

    @ApiProperty({example: "2024-06-20T08:08:00.191Z"})
    @UpdateDateColumn()
    updatedAt: Date;

    @ApiProperty({type: [Role]})
    @ManyToMany(() => Role)
    @JoinTable()
    roles: Role[]

    @OneToMany(() => Project, (project) => project.user)
    projects: Project[]
}