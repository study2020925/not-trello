import { ApiProperty } from "@nestjs/swagger";
import { IsInt, IsNotEmpty, IsString } from "class-validator";

export class CreateListDto {

    @ApiProperty({example: "In progress"})
    @IsNotEmpty({message: "Required"})
    @IsString({message: "Must be a string"})
    readonly name: string;
}