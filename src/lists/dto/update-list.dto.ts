import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";

export class UpdateListDto {

    @ApiProperty({example: "In progress", required: false})
    @IsOptional()
    @IsString({message: "Must be a string"})
    readonly name?: string;
}