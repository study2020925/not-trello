import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { List } from './lists.entity';
import { Repository } from 'typeorm';
import { CreateListDto } from './dto/create-list.dto';
import { UpdateListDto } from './dto/update-list.dto';
import { ProjectsService } from 'src/projects/projects.service';
import { UpdateListSortingDto } from './dto/update-list-sorting.dto';

@Injectable()
export class ListsService {
    
    constructor(@InjectRepository(List) private listsRepository: Repository<List>,
                private projectsService: ProjectsService) {}

    async getListsList(userId:number, projectId: number) {
        return await this.listsRepository.find({
            where: {
                project: {
                    id: projectId,
                    user: {id: userId}
                }            
            },
            order: {
                sortIndex: "ASC"
            },
            relations: {
               tasks: {comments: true},
            },
        })
    }

    async getListById(userId: number, id: number) {
        return await this.listsRepository.findOne({
            where: {
                id,
                project: {
                    //id: projectId,
                    user: {id: userId}
                }
            },
            relations: {
               project: true,
               tasks: {comments: true},
            },
        })
    }

    async createList(userId: number, projectId: number, createListDto: CreateListDto) {
        const newList = this.listsRepository.create(createListDto)
        const project = await this.projectsService.getProjectById(userId, projectId)
        newList.sortIndex = await this.getNextSortingIndex(projectId)

        await this.updateSorting(project.id, -1, newList.sortIndex)

        newList.project = project
        await this.listsRepository.save(newList)

        return newList
    }

    async updateList(userId: number, id: number, updateListDto: UpdateListDto) {
        const list = await this.getListById(userId, id) 
        const updatedList: List = {...list, ...updateListDto}

        await this.listsRepository.save(updatedList)
        
        return updatedList
    }

    // TODO: limit maximum sorting index
    async updateListSorting(userId: number, id: number, updateSortingDto: UpdateListSortingDto) {
        const list = await this.getListById(userId, id) 
        const oldIndex = list.sortIndex
        list.sortIndex = updateSortingDto.sortIndex

        await this.updateSorting(list.project.id, oldIndex, list.sortIndex)

        await this.listsRepository.save(list)
        
        return list
    }

    async deleteList(userId: number, id: number) {
        const list = await this.getListById(userId, id)
        if(!list) {
            return []
        }

        const projectId = list.project.id
        const oldIndex = list.sortIndex
        await this.listsRepository.remove(list)
        await this.updateSorting(projectId, oldIndex, -1)

        return this.getListsList(userId, projectId) 
    }
    
    // TODO: make sorting helper
    private async updateSorting(projectId: number, oldIndex: number, newIndex: number) {

        if(oldIndex === newIndex || oldIndex === -1) { return } 

        const query = this.listsRepository
            .createQueryBuilder()
            .update(List);

        if(newIndex === -1 || newIndex > oldIndex) { //delete or move down
            query
                .set({
                    sortIndex: () => "sortIndex - 1"
                })
                .where("sortIndex > :oldIndex", {oldIndex});

            if(newIndex > oldIndex) {
                query
                    .andWhere("sortIndex <= :newIndex", {newIndex});
            }
        } else if(newIndex < oldIndex) { //move up
            query
                .set({
                    sortIndex: () => "sortIndex + 1"
                })
                .where("sortIndex >= :newIndex", {newIndex});

            if(newIndex < oldIndex) {
                query.andWhere("sortIndex < :oldIndex", {oldIndex});
            }
        }

        await query
            .andWhere("project.id = :projectId", {projectId})
            .execute();
    }

    private async getNextSortingIndex(projectId: number) {
        const maxIndex = await this.listsRepository
            .createQueryBuilder("lists")
            .select("MAX(lists.sortIndex)", "value")
            //.from(List, "lists")
            .where("lists.project.id = :projectId", {projectId})
            .getRawOne();

        return maxIndex.value + 1 // null + 1 works fine for now
    }
}
