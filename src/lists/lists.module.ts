import { Module } from '@nestjs/common';
import { ListsController } from './lists.controller';
import { ListsService } from './lists.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { List } from './lists.entity';
import { ProjectsModule } from 'src/projects/projects.module';

@Module({
  controllers: [ListsController],
  providers: [ListsService],
  imports: [
    TypeOrmModule.forFeature([List]),
    ProjectsModule
  ],
  exports: [ListsService]
})
export class ListsModule {}
