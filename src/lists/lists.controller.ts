import { Body, Controller, Delete, Get, Param, Post, Put, Request } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { ListsService } from './lists.service';
import { List } from './lists.entity';
import { Roles } from 'src/auth/roles-auth.decorator';
import { CreateListDto } from './dto/create-list.dto';
import { UpdateListDto } from './dto/update-list.dto';
import { UpdateListSortingDto } from './dto/update-list-sorting.dto';

@ApiTags("Lists")
@ApiBearerAuth()
@Controller() //'projects/:projectId/lists'
export class ListsController {

    constructor(private listsService: ListsService) {}

    @ApiOperation({summary: "List lists"})
    @ApiParam({name: "projectId", type: "number", required: true, description: "Project id"})
    @ApiOkResponse({type: [List]})
    @Get('projects/:projectId/lists')
    @Roles(["USER"])
    getAll(@Request() req: any, @Param('projectId') projectId: number) {
        return this.listsService.getListsList(req.user.id, projectId)
    }

    @ApiOperation({summary: "Get list by id"})
    @ApiParam({name: "id", type: "number", required: true, description: "List id"})
    @ApiOkResponse({type: List})
    @Get("lists/:id")
    @Roles(["USER"])
    getById(@Request() req: any, @Param('id') id: number) {
        return this.listsService.getListById(req.user.id, id)
    }

    @ApiOperation({summary: "Create list"})
    @ApiParam({name: "projectId", type: "number", required: true, description: "Project id"})
    @ApiCreatedResponse({type: List})
    @Post('projects/:projectId/lists')
    @Roles(["USER"])
    create(@Request() req: any, @Param('projectId') projectId: number, @Body() createListDto: CreateListDto) {
        return this.listsService.createList(req.user.id, projectId, createListDto)
    }

    @ApiOperation({summary: "Update list"})
    @ApiParam({name: "id", type: "number", required: true, description: "List id"})
    @ApiOkResponse({type: List})
    @Put("lists/:id")
    @Roles(["USER"])
    update(@Request() req: any, @Param('id') id: number, @Body() updateListDto: UpdateListDto) {
        return this.listsService.updateList(req.user.id, id, updateListDto)
    }

    @ApiOperation({summary: "Update list sorting index"})
    @ApiParam({name: "id", type: "number", required: true, description: "List id"})
    @ApiOkResponse({type: List})
    @Put("lists/:id/sort")
    @Roles(["USER"])
    updateSorting(@Request() req: any, @Param('id') id: number, @Body() updateListSortingDto: UpdateListSortingDto) {
        return this.listsService.updateListSorting(req.user.id, id, updateListSortingDto)
    }

    @ApiOperation({summary: "Delete list"})
    @ApiParam({name: "id", type: "number", required: true, description: "List id"})
    @ApiOkResponse({type: [List]})
    @Delete("lists/:id")
    @Roles(["USER"])
    delete(@Request() req: any, @Param('id') id: number) {
        return this.listsService.deleteList(req.user.id, id)
    }
}
