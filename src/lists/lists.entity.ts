import { ApiProperty } from "@nestjs/swagger";
import { Project } from "../projects/projects.entity";
import { Task } from "../tasks/tasks.entity";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class List {

    @ApiProperty({example: 1})
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({example: "In progress"})
    @Column()
    name: string;

    @ApiProperty({example: 1})
    @Column({default: 1})
    sortIndex: number;

    @ApiProperty({example: "2024-06-20T08:08:00.191Z"})
    @CreateDateColumn()
    createdAt: Date;

    @ApiProperty({example: "2024-06-20T08:08:00.191Z"})
    @UpdateDateColumn()
    updatedAt: Date;

    //@ApiProperty({type: () => Project})
    @ManyToOne(() => Project, (project) => project.lists, { onDelete: "CASCADE" })
    @JoinColumn()
    project: Project

    @ApiProperty({type: () => [Task]})
    @OneToMany(() => Task, (task) => task.list)
    tasks: Task[]
}