import { ApiProperty } from "@nestjs/swagger";
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Role {

    @ApiProperty({example: 1})
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({example: "USER"})
    @Column()
    value: string;

    @ApiProperty({example: "Simple user"})
    @Column()
    description: string;

    @ApiProperty({example: "2024-06-20T08:08:00.191Z"})
    @CreateDateColumn()
    createdAt: Date;

    @ApiProperty({example: "2024-06-20T08:08:00.191Z"})
    @UpdateDateColumn()
    updatedAt: Date;
}