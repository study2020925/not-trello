import { Injectable } from '@nestjs/common';
import { Role } from './roles.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateRoleDto } from './dto/create-role.dto';

@Injectable()
export class RolesService {
    constructor(@InjectRepository(Role) private rolesRepository: Repository<Role>) {}

    async createRole(roleDto: CreateRoleDto) {
        const newRole = this.rolesRepository.create(roleDto)
        await this.rolesRepository.save(newRole)
        return newRole
    }

    async getRoleByValue(value: string) {
        return await this.rolesRepository.findOne({where: {value}})
    }
}
