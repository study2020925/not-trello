import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class CreateRoleDto {

    @ApiProperty({example: "USER"})
    @IsNotEmpty({message: "Required"})
    @IsString({message: "Must be a string"})
    readonly value: string;

    @ApiProperty({example: "Simple user"})
    @IsNotEmpty({message: "Required"})
    @IsString({message: "Must be a string"})
    readonly description: string;
}