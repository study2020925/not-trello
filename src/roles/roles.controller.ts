import { Body, Controller, Get, HttpStatus, Param, Post } from '@nestjs/common';
import { RolesService } from './roles.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { Roles } from 'src/auth/roles-auth.decorator';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { Role } from './roles.entity';

@ApiTags("Roles")
@ApiBearerAuth()
@Controller('roles')
export class RolesController {
    constructor(private rolesService: RolesService) {}

    @ApiOperation({summary: "Create role"})
    @ApiOkResponse({type: Role})
    @Post()
    @Roles(["ADMIN"])
    create(@Body() roleDto: CreateRoleDto) {
        return this.rolesService.createRole(roleDto)
    }

    @ApiOperation({summary: "Get role by its value"})
    @ApiParam({name: "value", type: "string", required: true, description: "Role name"})
    @ApiOkResponse({type: Role})
    @Get("/:value")
    @Roles([])
    getByValue(@Param('value') value: string) {
        return this.rolesService.getRoleByValue(value)
    }
}
