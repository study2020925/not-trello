import { ForbiddenException, HttpException, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { LoginDto } from './dto/login.dto';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/users/users.entity';
import * as bcrypt from 'bcryptjs';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { TokenDto } from './dto/token.dto';

@Injectable()
export class AuthService {

    constructor(private usersService: UsersService,
                private jwtService: JwtService
    ) {}

    async login(loginDto: LoginDto) {
        const user = await this.validateUser(loginDto)

        return this.generateToken(user)
    }
    
    async registration(registrationDto: CreateUserDto) {
        const candidate = await this.usersService.getUserByEmail(registrationDto.email)
        if(candidate) {
            throw new HttpException('User already exists', HttpStatus.BAD_REQUEST)
        }

        const user = await this.usersService.createUser(registrationDto)
        return this.generateToken(user)
    }

    private async generateToken(user: User) {
        const payload = {email: user.email, id: user.id, roles: user.roles}
        const token: TokenDto = {
            token: this.jwtService.sign(payload)
        }

        return token
    }

    private async validateUser(loginDto: LoginDto) {
        const user = await this.usersService.getUserByEmail(loginDto.email)
        if(!user) {
            throw new UnauthorizedException({message: "Invalid email"})
        }

        const passwordMatch = await bcrypt.compare(loginDto.password, user.password)
        if(!passwordMatch) {          
            throw new UnauthorizedException({message: "Invalid password"})
        }
        
        if(!user.isActive) {
            throw new ForbiddenException({message: "User isn`t active"})
        }

        return user
    }
}
