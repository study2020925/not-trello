import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable, UnauthorizedException } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { JwtService } from "@nestjs/jwt";
import { Observable } from "rxjs";
import { Roles } from "./roles-auth.decorator";

@Injectable()
export class RolesGuard implements CanActivate {
    
    constructor(private jwtService: JwtService,
                private reflector: Reflector) {}

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        try {
            const requiredRoles = this.reflector.getAllAndOverride<string[]>(Roles, [context.getHandler(), context.getClass()]);
            // when Roles decorator is not used
            if(requiredRoles === undefined) {
                return true;
            }

            const req = context.switchToHttp().getRequest();
            const authHeader = req.headers.authorization;
            const [bearer, token] = authHeader.split(' ')

            if(bearer !== "Bearer" || !token) {
                throw new UnauthorizedException({message: "User unauthorized"})
            }

            const user = this.jwtService.verify(token)  
            req.user = user

            // when there are no Roles limitations i.e. allow for authorized
            if(requiredRoles.length === 0) {
                return true;
            }
            return user.roles.some(role => requiredRoles.includes(role.value))

        }
        catch (e) {
            throw new HttpException("User unauthorized", HttpStatus.FORBIDDEN)
        }
    }
}