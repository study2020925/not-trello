import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class TokenDto {

    @ApiProperty({example: "[JWT access token]"})
    @IsNotEmpty({message: "Required"})
    readonly token: string;
}