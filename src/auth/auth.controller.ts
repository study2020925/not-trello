import { Body, Controller, HttpStatus, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { TokenDto } from './dto/token.dto';

@ApiTags("Auth")
@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) {}

    @ApiOperation({summary: "Authorization"})
    @ApiOkResponse({type: TokenDto})
    @Post('/login')
    login(@Body() loginDto: LoginDto) {
        return this.authService.login(loginDto)
    }

    @ApiOperation({summary: "Create user and authorize"})
    @ApiOkResponse({type: TokenDto})
    @Post('/registration')
    registration(@Body() registrationDto: CreateUserDto) {
        return this.authService.registration(registrationDto)
    }
}
