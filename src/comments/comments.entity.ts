import { ApiProperty } from "@nestjs/swagger";
import { Task } from "../tasks/tasks.entity";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Comment {

    @ApiProperty({example: 1})
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({example: "customer@email.com"})
    @Column()
    author: string;

    @ApiProperty({example: "Do a barrel roll"})
    @Column()
    content: string;

    @ApiProperty({example: "2024-06-20T08:08:00.191Z"})
    @CreateDateColumn()
    createdAt: Date;

    @ApiProperty({example: "2024-06-20T08:08:00.191Z"})
    @UpdateDateColumn()
    updatedAt: Date;

    //@ApiProperty({type: () => Task})
    @ManyToOne(() => Task, (task) => task.comments, { onDelete: "CASCADE" })
    @JoinColumn()
    task: Task
}