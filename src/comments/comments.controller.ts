import { Body, Controller, Delete, Get, Param, Post, Put, Request } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { CommentsService } from './comments.service';
import { Roles } from 'src/auth/roles-auth.decorator';
import { Comment } from './comments.entity';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';

@ApiTags("Comments")
@ApiBearerAuth()
@Controller()
export class CommentsController {

    constructor(private commentsService: CommentsService) {}

    @ApiOperation({summary: "List comments"})
    @ApiParam({name: "taskId", type: "number", required: true, description: "Task id"})
    @ApiOkResponse({type: [Comment]})
    @Get('tasks/:taskId/comments')
    @Roles(["USER"])
    getAll(@Request() req: any, @Param('taskId') taskId: number) {
        return this.commentsService.getCommentsList(req.user.id, taskId)
    }

    @ApiOperation({summary: "Get comment by id"})
    @ApiParam({name: "id", type: "number", required: true, description: "Comment id"})
    @ApiOkResponse({type: Comment})
    @Get("comments/:id")
    @Roles(["USER"])
    getById(@Request() req: any, @Param('id') id: number) {
        return this.commentsService.getCommentById(req.user.id, id)
    }

    @ApiOperation({summary: "Create comment"})
    @ApiParam({name: "taskId", type: "number", required: true, description: "Task id"})
    @ApiCreatedResponse({type: Comment})
    @Post('tasks/:taskId/comments')
    @Roles(["USER"])
    create(@Request() req: any, @Param('taskId') taskId: number, @Body() createCommentDto: CreateCommentDto) {
        return this.commentsService.createComment(req.user.id, taskId, createCommentDto)
    }

    @ApiOperation({summary: "Update comment"})
    @ApiParam({name: "id", type: "number", required: true, description: "Comment id"})
    @ApiOkResponse({type: Comment})
    @Put("comments/:id")
    @Roles(["USER"])
    update(@Request() req: any, @Param('id') id: number, @Body() updateCommentDto: UpdateCommentDto) {
        return this.commentsService.updateComment(req.user.id, id, updateCommentDto)
    }

    @ApiOperation({summary: "Delete comment"})
    @ApiParam({name: "id", type: "number", required: true, description: "Comment id"})
    @ApiOkResponse({type: [Comment]})
    @Delete("comments/:id")
    @Roles(["USER"])
    delete(@Request() req: any, @Param('id') id: number) {
        return this.commentsService.deleteComment(req.user.id, id)
    }
}
