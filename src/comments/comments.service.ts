import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Comment } from './comments.entity';
import { TasksService } from 'src/tasks/tasks.service';
import { Repository } from 'typeorm';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';

@Injectable()
export class CommentsService {

    constructor(@InjectRepository(Comment) private commentsRepository: Repository<Comment>,
                private tasksService: TasksService) {}

    async getCommentsList(userId: number, taskId: number) {
        return await this.commentsRepository.find({
            where: {
                task: {
                    id: taskId,
                    list: {
                        project: {
                            user: {id: userId}
                        } 
                    }
                }                      
            },
            order: {
                createdAt: "DESC"
            }
        })
    }

    async getCommentById(userId: number, id: number) {
        return await this.commentsRepository.findOne({
            where: {
                id: id,
                task: {
                    list: {
                        project: {
                            user: {id: userId}
                        } 
                    }
                }
            },
            relations: {
                task: true
            },
        })
    }

    async createComment(userId: number, taskId: number, createCommentDto: CreateCommentDto) {
        const newComment = this.commentsRepository.create(createCommentDto)
        newComment.task = await this.tasksService.getTaskById(userId, taskId)
        await this.commentsRepository.save(newComment)

        return newComment
    }

    async updateComment(userId: number, id: number, updateCommentDto: UpdateCommentDto) {
        const comment = await this.getCommentById(userId, id) 
        const updatedComment: Comment = {...comment, ...updateCommentDto}

        await this.commentsRepository.save(updatedComment)
        
        return updatedComment
    }

    async deleteComment(userId: number, id: number) {
        const comment = await this.getCommentById(userId, id)
        if(!comment) {
            return []
        }

        const taskId = comment.task.id
        await this.commentsRepository.remove(comment)

        return this.getCommentsList(userId, taskId) 
    }
}
