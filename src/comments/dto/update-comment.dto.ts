import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class UpdateCommentDto {
    @ApiProperty({example: "Do a barrel roll", required: true})
    @IsNotEmpty({message: "Required"})
    @IsString({message: "Must be a string"})
    readonly content: string;
}