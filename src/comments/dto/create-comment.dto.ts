import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString } from "class-validator";

export class CreateCommentDto {

    @ApiProperty({example: "customer@email.com"})
    @IsNotEmpty({message: "Required"})
    @IsEmail()
    readonly author: string;

    @ApiProperty({example: "Do a barrel roll", required: true})
    @IsNotEmpty({message: "Required"})
    @IsString({message: "Must be a string"})
    readonly content: string;
}