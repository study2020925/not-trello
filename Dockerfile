FROM node:22.5.1

WORKDIR /app
COPY . .
RUN npm install

EXPOSE $PORT

CMD ["npm", "run", "start"]