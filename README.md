<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

<p align="center">Simple Nest.js task manager built as test task </p>

## Description

To get help:
```
http://[host]:[port]/help
```

## Installation

```bash
$ docker compose up
$ npm run seed
```